// BASE SETUP
// =============================================================================

//extend Array def
Array.prototype.contains = function(k, callback) {
    var self = this;
    return (function check(i) {
        if (i >= self.length) {
            return callback(false);
        }

        if (self[i] === k) {
            return callback(true);
        }

        return process.nextTick(check.bind(null, i+1));
    }(0));
}


//config
var config = require('./app/config');

// call the packages we need
var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var httpHeader = require('./middleware/httpHeader/httpHeader');


var app = express();

// var Authentication = require('../../middleware/authentication/authentication');

//DATABASE CONNECTION
mongoose.connect(config.db); 

var port = process.env.PORT || 5000; // set our port

//Front Middleware
app.use(bodyParser());
app.use(httpHeader);

//app.use(Authentication);

//ROUTERS

//Default routes
app.get('/',function(req, res){
   res.send(config.app); 
   console.log('default routes /');
});
var signRouter = require('./app/routers/signRouter');
var userRouter = require('./app/routers/userRouter');
var talkRouter = require('./app/routers/talkRouter');

app.use('/web_api', signRouter);
app.use('/web_api', userRouter);
app.use('/web_api', talkRouter);

////Routes with authentication
//var activityRouter = require('./app/routers/activityRouter');
//app.use('/activity', activityRouter);
//
//var userRouter = require('./app/routers/userRouter');
//app.use('/user', userRouter);
//
//var notificationRouter = require('./app/routers/notificationRouter');
//app.use('/notification', notificationRouter);


//Public routes
//var signRouter = require('./signin/routers/signRouter');
//app.use('/', signRouter);


// START THE SERVER
// =============================================================================
app.listen(port);

console.log('-----------------------------------------------------');
console.log('App "'+config.app+'" load on port: ' + port);
console.log('NODE_ENV: '+process.env.NODE_ENV || 'DEFAULT');
console.log('-----------------------------------------------------');
