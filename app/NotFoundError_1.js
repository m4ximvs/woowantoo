/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var util = require("util");

function NotFoundError(message) {
  Error.call(this);
  Error.captureStackTrace(this, arguments.callee);
  this.message = message;
  this.name = 'NotFoundError';
  this.statusCode = 404;
}

util.inherits(NotFoundError, Error);


module.exports = NotFoundError;