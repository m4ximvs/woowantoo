
var DEV_CONF = {
    app: 'doitbuzz',
    db:  'mongodb://doitbuzz_dba:re4dy2go@ds055980.mongolab.com:55980/doitbuzz_dev'
};

var PROD_CONF = {
    app: 'doitbuzz',
    db:  'mongodb://doitbuzz_dba:re4dy2go@ds055980.mongolab.com:55980/doitbuzz_dev'
};

var config = function(){
    switch(process.env.NODE_ENV){
        case 'development':
            return DEV_CONF;

        case 'production':
            return PROD_CONF;

        default:
            return PROD_CONF;
    }
}

module.exports = config();

