var util = require("util");
var NotFoundError = require('../NotFoundError');
var ErrorHandler = require('../handler/errorHandler');
var ActivityModule = require('../models/activity');
var Activity = ActivityModule.model;
var UserModule = require('../models/user');
var User = UserModule.model;

var ActivityController = function() {
//    this.req = req;
//    this.res = res;
//    this.data = JSON.parse(req.body.data);
};

/**
 * ADD New
 * @param {type} req
 * @param {type} res
 * @returns {undefined}
 */
ActivityController.prototype.add = function(callback) {
//    var that = this;
    var activity = new Activity(data);
    activity.save(callback);
}

/**
 * Get All 
 * @param {type} req
 * @param {type} res
 * @returns {undefined}
 */
ActivityController.prototype.getAll = function(callback) {
    Activity.find(callback);
}

/**
 * GET by id
 * @param {id} activity_id
 * @param {function} callback
 * @returns {void}
 */
ActivityController.prototype.get = function(activity_id, callback) {
    Activity.findById(activity_id, function(err, activity) {
        if (!activity)
            err = new NotFoundError("ID not found");
        callback(err, activity);
    });
}

ActivityController.prototype.getToUser = function(user_id, callback) {
    Activity.find({"destinations._id": user_id}, function(err, activity) {
        if (!activity)
            err = new NotFoundError("ID not found");
        callback(err, activity);
    });
}

ActivityController.prototype.getFromUser = function(user_id, callback) {
    Activity.find({"sender.lastname": user_id}, function(err, activity) {
        if (!activity)
            err = new NotFoundError("ID not found");
        callback(err, activity);
    });
}

/**
 * UPDATE by id
 * @param {id} activity_id
 * @param {activity/json} updActivity
 * @param {function} callback
 * @returns {void}
 */
ActivityController.prototype.update = function(activity_id, updActivity, callback) {
    Activity.findOneAndUpdate({_id: activity_id}, updActivity, {new : true, upsert: false}, function(err, activity) {
        if (!activity)
            err = new NotFoundError("ID not found");
        callback(err, activity);
    });
}

/**
 * REMOVE by id
 * @param {id} activity_id
 * @param {function} callback
 * @returns {void}
 */
ActivityController.prototype.delete = function(activity_id, callback) {
    Activity.remove({_id: activity_id}, function(err, activity) {
        if (!activity)
            err = new NotFoundError("ID not found");
        callback(err, activity);
    });
}

//ActivityController.prototype.geo = function(location, callback) {   
//    Activity.geoNear(location.coordinates, {maxDistance: location.circ, spherical: false, num: 10}, function(err, results, stats) {
//        console.log(results);
//        callback(err, results);
//    });
//}



module.exports = ActivityController;