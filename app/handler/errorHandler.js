
var ErrorHandler = function(err, res) {
    errorLog = JSON.stringify(err, ['name', 'message', 'stack'], 3);
    console.log(errorLog);
    res.json({error: err, error_log: errorLog});
}

module.exports = ErrorHandler;