var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var woo_tools = require('../../module/wootools/wootools');

var User = require('./user');
var UserModel = User.model;

var Talk = require('./talk');
var TalkModel = Talk.model;

var Location = require('./location');

var NotificationModule = require('./notification');
var Notification = NotificationModule.model;

var options = {
};

//The Schema
var ActivitySchema = new Schema({
    _sender: {type: Schema.Types.ObjectId, ref: 'User'},
    isActive: {type: Boolean, default: true},
    _signaledBy: [{type: Schema.Types.ObjectId, ref: 'User'}],
    category: {type: String, enum: [
            'culture',
            'dance',
            'drink',
            'food',
            'game',
            'help',
            'love',
            'misc',
            'movie',
            'music',
            'outdoor',
            'party',
            'relax',
            'shopping',
            'sport',
            'travel'
        ], default: 'misc'},
    title: {type: String, default: null},
    description: {type: String, default: null},
    date: {type: Date, default: Date.now},
    creation_date: {type: Date, default: Date.now},
    coordinates: [Number], //{type: [Number], index: '2d'},
    location: {type: String, default: null},
    max_suscriptions: {type: Number, default: 0},
    destinations: [{type: Schema.Types.ObjectId, ref: 'User'}],
    public: {type: Boolean, default: true},
    propagation: {
        toAllFriends: {type: Boolean, default: true},
        sex: {type: String, enum: ['m', 'w', 'all'], default: 'all'},
        age_min: {type: Number, default: 0},
        age_max: {type: Number, default: 99},
        adult: {type: Boolean, default: true}
    },
    notification: {
        talk: {type: Boolean, default: false},
        suscription: {type: Boolean, default: false},
    },
    stats: {
        views: {type: Number, default: 0},
        geoViews: {type: Number, default: 0},
        detailViews: {type: Number, default: 0}
    },
    suscriptions: [{type: Schema.Types.ObjectId, ref: 'User'}],
    suscriptions_blocked: [{type: Schema.Types.ObjectId, ref: 'User'}],
    talks: [Talk.schema]
}, options);

//schema index defintion
ActivitySchema.index({"coordinates": "2dsphere"});

/*****************************************************************************************************************
 * STATIC METHODS
 */

//geoSearch
ActivitySchema.statics.geo = function(user, location, callback) {
    
    var usr_age = woo_tools.calculateAge(user.public_profile.birthdate);
    console.log('AGE->');
    console.log(usr_age);
        
    var date_condition = {"$gte": new Date()};
    
    var condition;
    if(!user.public_profile.gender)
        condition = {
            date: date_condition,
            isActive: true,
            public: true,
            //age
            //"$and": [{'propagation.age_min': {"$gte": usr_age}}, {'propagation.age_max': {"$lt": usr_age}}],
            'propagation.age_min': {"$lte": usr_age},
            'propagation.age_max': {"$gt": usr_age},
            _sender: {"$ne": user._id},
            suscriptions: {"$ne": user._id}
        };
    else
        condition = {
            date: date_condition,
            isActive: true,
            public: true,
            //sex
            "$or": [{"propagation.sex": user.public_profile.gender}, {"propagation.sex": "all"}],
            'propagation.age_min': {"$lte": usr_age},
            'propagation.age_max': {"$gt": usr_age},

            _sender: {"$ne": user._id},
            suscriptions: {"$ne": user._id}
        };
    
    console.log(condition);
    console.log(location.coordinates);
    console.log(location.circ);
    this.geoNear(location.coordinates, {maxDistance: location.circ / 6371, spherical: true, num: 10, query: condition}, function(err, results, stats) {
        callback(err, results);
    });
};


//build activity - _sender
ActivitySchema.statics.get = function(conditions, field, sort, callback) {
    var option = {
        path: '_sender',
        select: 'public_profile'
    };

    this.find(conditions, field, sort).populate(option).exec(function(err, activities) {
        callback(err, activities);
    });
}

//build activity  -_sender
ActivitySchema.statics.getById = function(act_id, callback) {

    var option = [
        {path: '_sender', select: 'public_profile'},
        {path: 'suscriptions', select: 'public_profile'}
    ];

    this.findById(act_id).populate(option).exec(function(err, activities) {
        callback(err, activities);
    });
}



/*****************************************************************************************************************
 *  INSTANCE METHODS
 */

// Sedner
ActivitySchema.methods.getSender = function(callback) {
    var that = this;
    if (that.sender_email)
        UserModel.findByEmail(this.sender_email, function(err, user) {
            that.sender = user.public_profile;
            callback(err, that);
        });
    else
        callback(null, that);

};


// TALKS
ActivitySchema.methods.addTalk = function(talk, callback) {
    this.talks.push(talk);
    this.save(callback);
};

ActivitySchema.methods.getTalks = function(callback) {
    var that = this;
    var build_talks = [];

    if (that.talks.length > 0) {
        for (var i = 0; i < that.talks.length; i++) {
            TalkModel.getBuildTalk(that.talks[i], function(err, talk) {
                build_talks.push(talk);

                if (build_talks.length === that.talks.length) {
                    callback(err, build_talks);
                    console.log(build_talks);
                }
            });
        }
    }
    else
        callback(null, build_talks);
};

//SUSCRIPTIONS
ActivitySchema.methods.addSubscription = function(subscription, callback) {
    var that = this;
    var added = that.suscriptions.addToSet(subscription);
    if (added)
        that.save(callback);
    else
        callback({message: 'Error - already present'}, that);
};

ActivitySchema.methods.delSubscription = function(subscription, callback) {

    this.suscriptions.remove(subscription);

    this.save(function(err, activity) {
        callback(err, activity);
    });

};

//Virtual
//ActivitySchema.virtual('alreadySuscribed').get(function () {
//    if(this.suscriptions.id())
//  return ;
//});

module.exports.schema = ActivitySchema;
module.exports.model = mongoose.model('Activity', ActivitySchema);