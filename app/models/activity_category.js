var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ActivityCategorySchema = new Schema({

    name: {type: String, index: true},
    label: {
        fr: String,
        de: String,
        en: String,
        it: String
    }
});

module.exports.schema = ActivityCategorySchema;
module.exports.model = mongoose.model('ActivityCategory', ActivityCategorySchema);