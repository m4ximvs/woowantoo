var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var FriendSchema = new Schema({
    _owner: { type: Schema.Types.ObjectId, ref: 'User' },
    _friend: { type: Schema.Types.ObjectId, ref: 'User' },
    
    state:  { type: String, enum: ['in_demand', 'action_required', 'approved', 'denied'] },
    
    isActive: {type: Boolean, default: true},
        
    source: String,
    sync_date: {type: Date, default: Date.now},
    creation_date: {type: Date, default: Date.now}
});

FriendSchema.index({_owner: 1, _friend: 1}, {unique: true});

//// Sender
//FriendSchema.statics.getById = function(friend_id, callback) {
//
//    var option = {
//        path: '_user',
//        select: 'public_profile'
//    };
//    
//    this.findById(friend_id).populate(option).exec(function(err, friend) {
//        callback(err, friend);
//    });
//};

FriendSchema.statics.get = function(condition, field, sort, callback) {

    var populate_option = {
        path: '_friend',
        select: 'public_profile'
    };
    
    this.find(condition, field, sort).populate(populate_option).exec(function(err, friend) {
        callback(err, friend);
    });
};

FriendSchema.statics.getOne = function(condition, field, sort, callback) {

    var populate_option = {
        path: '_friend',
        select: 'public_profile'
    };
    
    this.findOne(condition, field, sort).populate(populate_option).exec(function(err, friend) {
        callback(err, friend);
    });
};

//FriendSchema.statics.getFriendIds = function(condition, field, sort, callback) {
//
//    var populate_option = {
//        path: '_friend',
//        select: 'public_profile'
//    };
//    
//    this.findOne(condition, field, sort).populate(populate_option).exec(function(err, friend) {
//        callback(err, friend);
//    });
//};
//
//FriendSchema.statics.unactiveFriends = function(condition, field, sort, callback) {
//    
//    this.find(condition, field, sort).populate(populate_option).exec(function(err, friend) {
//        callback(err, friend);
//    });
//};




module.exports.schema = FriendSchema;
module.exports.model = mongoose.model('Friend', FriendSchema);