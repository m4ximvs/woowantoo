var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var LocationSchema   = new Schema({
	x: String,
        y: String,
        circ: Number
});

module.exports.schema = LocationSchema;
module.exports.model = mongoose.model('location', LocationSchema);