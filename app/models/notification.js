var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserModule = require('./user');
var User = UserModule.model;

var NotificationSchema = new Schema({
    type: {type: String, default: 'subscription'}, //allowed value: friend; subscription; talk
    message: String,
    target_user: {type: String, index: true},
    
    src_user: {type: String, index: true},
    src_user_data: {
        firstname: String,
        lastname: String,
        email: String,
        birthdate: Date,
        photo: String,
        lang: String,
        city: String,
        country: String
    },
    target_activity: {type: Schema.Types.ObjectId, ref: 'Activity'}, //refer only if type != friend

    target_activity_data: {
        sender_email: {type: String},
        isActive: {type: Boolean},
        category: String,
        title: String,
        description: String,
        date: {type: Date},
        creation_date: {type: Date}
    },
  
    
    status: {
        read: {type: Boolean, default: Boolean.false},
        pushed: {type: Boolean, default: Boolean.false},
        masked: {type: Boolean, default: Boolean.false},
        actionRequired: {type: Boolean, default: Boolean.false}
    },
    date: {type: Date, default: Date.now}

});

////GET UNPUSHED NOTIFS
//NotificationSchema.statics.get = function(user_mail, callback ) {
//    this.find({'target_user': user_mail, 'status.pushed': false}, function(err, notifications) {
//        if (!notifications)
//            err = new NotFoundError("ID not found");
//        callback(err, notifications);
//    });
//};

////GET UNPUSHED NOTIFS
//NotificationSchema.statics.get = function(user_mail, act_id, callback ) {
//    this.find({'target_user': user_mail, 'target_activity': act_id, 'status.pushed': false}, function(err, notifications) {
//        if (!notifications)
//            err = new NotFoundError("ID not found");
//        callback(err, notifications);
//    });
//};

//GET UNPUSHED NOTIFS
NotificationSchema.statics.get = function(user_mail, callback ) {
    this.find({'target_user': user_mail, 'status.pushed': false}, function(err, notifications) {
        if (!notifications)
            err = new NotFoundError("ID not found");
        callback(err, notifications);
    });
};


//MARK NOTIF AS PUSHED
NotificationSchema.statics.markAsPushed = function(notif_id, callback ) {

    this.findById(notif_id, function(err, notification) {
        if (!notification)
            err = new NotFoundError("ID not found");

        notification.status.pushed = true;
        notification.save(callback(err, notification));

    });
};

//MARK NOTIF AS READ
NotificationSchema.statics.markAsRead = function(notif_id, callback ) {

    this.findById(notif_id, function(err, notification) {
        if (!notification)
            err = new NotFoundError("ID not found");

        notification.status.read = true;
        notification.save(callback(err, notification));

    });
};

/*****************************************************************************************************************
 *  INSTANCE METHODS
 */


NotificationSchema.methods.build_src_user_data = function(callback) {
    var that = this;
    
    User.findByEmail(that.src_user, function(err, build_user) {
        that.src_user_data = build_user;
        callback(null, that);
    });
    
};

module.exports.schema = NotificationSchema;
module.exports.model = mongoose.model('Notification', NotificationSchema);