/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//Module Dependencies
var http = require('http');
var express = require('express');
var NotFoundError = require('../NotFoundError');
var ErrorHandler = require('../handler/errorHandler');

var Authentication = require('../../middleware/authentication/authentication');

var encryption = require('../../module/encryption/encryption');

var UserModule = require('../models/user');
var User = UserModule.model;

var ActivityModule = require('../models/user');
var Activity = ActivityModule.model;

var FriendModule = require('../models/friend');
var Friend = FriendModule.model;

var NotificationModule = require('../models/notification');
var Notification = NotificationModule.model;

var ActivityService = require('../services/activityServices');

var wootools = require('../../module/wootools/wootools');

// ROUTES FOR OUR API
// =============================================================================

// create our router
var userRouter = express.Router();

// middleware called before each routes in this router
userRouter.use(function(req, res, next) {
    // do logging
    console.log('MIDDLEWARE - user router -> .' + req.originalUrl);
    next();
});

// this router need authentication
//userRouter.use(Authentication.handler);

userRouter.route('/')
        .get(function(req, res) {
            User.find(function(err, users) {
                if (err)
                    new ErrorHandler(err, res);
                res.json(users);
            });
        });


userRouter.route('/:user_email')
        .get(function(req, res) {
            var uid = req.params.user_email;
            User.findByEmail(uid, function(err, user) {
                if (err)
                    new ErrorHandler(err, res);
                else
                    res.json(user);
            });
        })
        /**
         * UPDATE USER
         */
        .put(function(req, res) {
            console.log('PUT FUNC: ' + req.body.data);

            var uid = req.params.user_email;

//            console.log("UID: " + uid);
//            console.log("TYPE: " + typeof(req.body.data));

            try {
                var updUser = JSON.parse(req.body.data);
            } catch (err) {
                new ErrorHandler(err, res);
            }

            //var updUser = req.body.data;

            var condition = {
                "email": uid
            };


            //Crypt the new password
            if (updUser.password) {
                encryption.crypt(updUser.password, function(err, hash) {
                    updUser.password = hash;
                    //update the user
                    User.findOneAndUpdate(condition, updUser, function(err, user) {
                        if (err)
                            new ErrorHandler(err, res);
                        else
                            res.json(user);
                    });
                });
            }
            else {
                //update the user
                User.findOneAndUpdate(condition, updUser, function(err, user) {
                    if (err)
                        new ErrorHandler(err, res);
                    else
                        res.json(user);
                });
            }

        });

//userRouter.route('/:user_email/validity_check/')
//        .get(function(req, res) {
//            var uid = req.params.user_email;
//            User.findByEmail(uid, function(err, user) {
//                if (err)
//                    new ErrorHandler(err, res);
//                else {
//                    if (user.fb_token) {
//                        console.log('TOKEN');
//                        console.log(user.fb_token);
//
//                        var request = require('request');
//                        var params = '';
//                        params += 'grant_type=fb_exchange_token';
//                        params += '&client_id='+'1488166564803051';
//                        params += '&client_secret='+'765434b98219062dd43ff22b92f11676';
//                        params += '&fb_exchange_token='+user.fb_token;
//                        
//                        request('https://graph.facebook.com/oauth/access_token?'+params, function(error, response, body) {
//                            if(!error && response.statusCode == 200) {
//                            var fb_res = body.split('&');
//                            fb_res.forEach(function(ele, i){
//                                var fb_res_array = ele.split('=');
//                                fb_res[i] = fb_res_array[1];
//                            });
//                           
//                            console.log(fb_res[0]); 
//                            res.json(fb_res[0]);
//                            }
//                        });
//                    }
//                }
//
//            });
//        });

//userRouter.route('/:user_email/update_pwd/')
//
//        .get(function(req, res) {
//            var uid = req.params.user_email;
//            User.findByEmail(uid, function(err, user) {
//                if (err)
//                    new ErrorHandler(err, res);
//                else {
//                    user.updatePassword();
//                    res.json(user);
//                }
//
//            });
//        });

userRouter.route('/:user_email/extend_token/')
        .get(function(req, res) {
            var uid = req.params.user_email;
            User.findByEmail(uid, function(err, user) {
                if (err)
                    new ErrorHandler(err, res);
                else {
                    user.extendFbToken();
                    res.json(user);
                }

            });
        });
userRouter.route('/:user_email/facebook_info/')

        .get(function(req, res) {
            var uid = req.params.user_email;
            User.findByEmail(uid, function(err, user) {
                if (err)
                    new ErrorHandler(err, res);
                else {
                    if (user.fb_token) {
                        console.log('TOKEN');
                        console.log(user.fb_token);

                        var request = require('request');
                        request('https://graph.facebook.com/me?access_token=' + user.fb_token, function(error, response, body) {


                            res.json(body);
                        });
                    }
                }

            });
        });

userRouter.route('/:user_email/facebook_friends/')

        .get(function(req, res) {
            var uid = req.params.user_email;
            User.findByEmail(uid, function(err, user) {
                if (err)
                    new ErrorHandler(err, res);
                else {
                    if (user.fb_token) {
                        console.log('TOKEN');
                        console.log(user.fb_token);

                        var request = require('request');
                        request('https://graph.facebook.com/me/friends?access_token=' + user.fb_token, function(error, response, body) {
                            try {
                                var body = JSON.parse(body);
                                var friends = [];
                                if (!body.error) {

                                    body.data.forEach(function(ele) {
                                        var friend = new Friend();
                                        friend.name = ele.name;
                                        friend.fb_id = ele.id;
                                        friend.source = 'fb';
                                        friend.active = true;

                                        friends.push(friend);
                                    });

                                    //save the found friends into the user
                                    user.friends = friends;
                                    user.save();
                                }


                            } catch (err) {
                                new ErrorHandler(err, res);
                            }


                            res.json(friends);
                        });
                    }
                }

            });
        });


userRouter.route('/friends/')

        .get(function(req, res) {
            var user_id = req.params.id;

            User.findById(user_id, function(err, user) {
                if (err)
                    new ErrorHandler(err);
                res.json(user);
            });
        });

userRouter.route('/:user_email/friends_demand')

        .post(function(req, res) {
            
            var user_email = req.params.user_email;
            try {
                var data = JSON.parse(req.body.data);
            } catch (err) {
                new ErrorHandler(err, res);
            }
            var target_email = data.target;

            //update the user
            
            User.do_friends_demand(user_email, target_email,function(err, src_user) {
                    if (err)
                        new ErrorHandler(err);
                    res.json(src_user);
            });
            
        });


userRouter.route('/:user_email/friends_approval')

        .post(function(req, res) {
            
            var user_email = req.params.user_email;
            try {
                var data = JSON.parse(req.body.data);
            } catch (err) {
                new ErrorHandler(err, res);
            }
            var target_email = data.target;

            //update the user
            
            User.do_friends_approval(user_email, target_email,function(err, src_user) {
                    if (err)
                        new ErrorHandler(err);
                    res.json(src_user);
            });
            
        });
        
userRouter.route('/:user_email/friends_delete')

        .delete(function(req, res) {
            
            var user_email = req.params.user_email;
            try {
                var data = JSON.parse(req.body.data);
            } catch (err) {
                new ErrorHandler(err, res);
            }
            var target_email = data.target;

            //update the user 
            User.delete_friend(user_email, target_email,function(err, src_user) {
                    if (err)
                        new ErrorHandler(err);
                    res.json(src_user);
            });
            
        });

userRouter.route('/:user_id/activities')
        .get(function() {

            var user_id = req.params.id;
            User.findById(user_id, function(err, user) {
                user.getMyActivities(user_id, function(err, activities) {
                    if (err)
                        new ErrorHandler(err);
                    res.json(activities);
                });
            });
        });
        
userRouter.route('/activities')

        // Get all activities for the user
        .post(function(req, res) {

            console.log('auth: ' + Authentication.store.user.email);
            console.log(Authentication.store.user.email);
            //Send all activities
            ActivityService.getMyActivities(Authentication.store.user, function(err, activities) {
                if (err)
                    new ErrorHandler(err, res);
                else
                    res.json(activities);
            });
        });
userRouter.route('/history')

        .get(function(req, res) {

        })
        .put(function(req, res) {

        })
        .delete(function(req, res) {

        });
module.exports = userRouter;

  