/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//Module Dependencies
var express = require('express');
var NotFoundError = require('../NotFoundError');
var ErrorHandler = require('../handler/errorHandler');
var ActivityController = require('../controllers/activityController');

var ActivityModule = require('../models/activity');
var Activity = ActivityModule.model;

var woo_tools = require('../../module/wootools/wootools');


// ROUTES FOR OUR API
// =============================================================================

// create our router
var activityRouter = express.Router();

// middleware called before each routes in this router
activityRouter.use(function(req, res, next) {
    // do logging
    console.log('MIDDLEWARE - Activity router -> .' + req.originalUrl);
    next();
});



//activityRouter.route('/')
//
//        //Route to add an activity
//        .post(function(req, res) {
//            console.log('post activity');
//
//            try {
//                var activityData = JSON.parse(req.body.data);
//            } catch (err) {
//                new ErrorHandler(err, res);
//            }
//
//            var activity = new Activity(activityData);
//
//            activity.save(function(err, activity) {
//                console.log('save');
//                if (err)
//                    new ErrorHandler(err, res);
//                else
//                    res.json({message: 'Activity successfully created!'});
//                console.log('finish post');
//            });
//        })
//
//
//activityRouter.route('/:act_id')
//
//        // get activiy by id
//        .get(function(req, res) {
//            var activity_id = req.params.act_id;
//
//            Activity.findById(activity_id).populate('entries').exec(function(err, activity) {
//                if (err)
//                    new ErrorHandler(err, res);
//                else {
//                    activity.getSender(function(err, build_activity) {
//                        res.json(build_activity);
//                    });
//                }
//            });
//        })
//
//        //update aactivity by id
//        .put(function(req, res) {
//            var activity_id = req.params.act_id;
//            try {
//                var updActivity = JSON.parse(req.body.data);
//            } catch (err) {
//                new ErrorHandler(err, res);
//            }
//
//            if (updActivity._id)
//                delete updActivity._id;
//
//            Activity.findOneAndUpdate(activity_id, updActivity, function(err, activities) {
//                if (err)
//                    new ErrorHandler(err, res);
//                else
//                    res.json(activities);
//            });
//        })
//
//        //delete activity by id
//        .delete(function(req, res) {
//            var activity_id = req.params.act_id;
//
//            Activity.remove(activity_id, function(err, activities) {
//                if (err)
//                    new ErrorHandler(err, res);
//                else
//                    res.json({message: 'Deleted'});
//            });
//        });


//activityRouter.route('/:id/talks')
//        //get all talks
//        .get(function(req, res) {
//            var act_id = req.params.id;
//
//            Activity.findById(act_id, function(err, activity) {
//                if (err)
//                    new ErrorHandler(err, res);
//                else {
//                    activity.getTalks(function(err, talks) {
//                        res.json(talks);
//                    });
//                }
//            });
//
//        })
//
//        //create new talks
//        .post(function(req, res) {
//            var act_id = req.params.id;
//
//            //contruct the talks obj
//            try {
//                var data = JSON.parse(req.body.data);
//            } catch (err) {
//                new ErrorHandler(err, res);
//            }
//
//
//            Activity.findById(act_id, function(err, activity) {
//                if (err)
//                    new ErrorHandler(err, res);
//                else {
//                    activity.addTalk(data, function(err, activity) {
//                        activity.getTalks(function(err, talks) {
//                            res.json(talks);
//                        });
//                    });
//                }
//
//
//            });


//        });


//Suscription to activity
//activityRouter.route('/:id/suscribe')
//        .get(function(req, res) {
//            var act_id = req.params.id;
//        })
//
//
//        .post(function(req, res) {
//            var act_id = req.params.id;
//
//            //contruct the talks obj
//            try {
//                var data = JSON.parse(req.body.data);
//            } catch (err) {
//                new ErrorHandler(err, res);
//            }
//
//
//            Activity.findById(act_id, function(err, activity) {
//                if (err)
//                    new ErrorHandler(err, res);
//                else {
//
//                    activity.addSubscription(data, function(err, activity) {
//                        if (err)
//                            new ErrorHandler(err);
//
//                        //build the embed sender obj before send
//                        activity.getSender(function(err, activity) {
//                            res.json(activity);
//                        });
//
//                    });
//                }
//
//
//            });
//
//
//        })
//
//        .delete(function(req, res) {
//            var act_id = req.params.id;
//
//            //contruct the talks obj
//            try {
//                var data = JSON.parse(req.body.data);
//            } catch (err) {
//                new ErrorHandler(err, res);
//            }
//
//
//            Activity.findById(act_id, function(err, activity) {
//                if (err)
//                    new ErrorHandler(err, res);
//                else {
//
//                    activity.delSubscription(data, function(err, activity) {
//                        if (err)
//                            new ErrorHandler(err);
//                        //build the embed sender obj before send
//                        activity.getSender(function(err, activity) {
//                            if (err)
//                                new ErrorHandler(err);
//                            res.json(activity);
//                        });
//                    });
//                }
//
//
//            });
//
//        });

//activityRouter.route('/:id/unactive')
//        .delete(function(req, res) {
//            var activity_id = req.params.id;
//
//            Activity.findOneAndUpdate({_id: activity_id}, {isActive: false}, function(err, activity) {
//                if (err)
//                    new ErrorHandler(err, res);
//                else
//                    res.json(activity);
//            });
//        });


/*****************************************************************************************************************
 * Activity Search
 */

////ROUTE TO QUERY Activity for a user
//activityRouter.route('/search/:user_email')
//        .get(function(req, res) {
//            var user_email = req.params.user_email;
//            var date_condition = {"$gte": new Date()};
//            var conditions = {
//                date: date_condition,
//                isActive: true,
//                $or: [{'sender_email': user_email}, {'suscriptions': user_email}]
//            };
//            var sort = {sort: 'date'};
//            Activity.find(conditions, null, sort, function(err, activities) {
//                if (err)
//                    new ErrorHandler(err);
//                //build senders
//                woo_tools.activity.build_senders(activities, function(activities) {
//                    res.json(activities);
//                });
//
//            });
//        });
//
////ROUTE TO QUERY Activity history for a user
//activityRouter.route('/search/:user_email/history')
//        .get(function(req, res) {
//            var user_email = req.params.user_email;
//            var date_condition = {"$lt": new Date()};
//            var conditions = {
//                date: date_condition,
//                isActive: true,
//                $or: [{'sender.email': user_email}, {'suscriptions': user_email}]
//            };
//            var sort = {sort: 'date'};
//            Activity.find(conditions, null, sort, function(err, activities) {
//                if (err)
//                    new ErrorHandler(err);
//
//                //build senders
//                woo_tools.activity.build_senders(activities, function(activities) {
//                    res.json(activities);
//                });
//            });
//        });
//
//
////ROUTE TO QUERY Firend Activity for a users
//activityRouter.route('/search/:user_email/friends')
//        .get(function(req, res) {
//            var user_email = req.params.user_email;
//
//            var date_condition = {"$gte": new Date()};
//            var conditions = {
//                date: date_condition,
//                isActive: true,
//                'destinations': user_email,
//                'suscriptions': {'$ne': user_email}
//            };
//            var sort = {sort: 'date'};
//
//            Activity.find(conditions, null, sort, function(err, activities) {
//                if (err)
//                    new ErrorHandler(err);
//                //build senders
//                woo_tools.activity.build_senders(activities, function(activities) {
//                    res.json(activities);
//                });
//            });
//        });
//
////ROUTE TO QUERY Activity by geolocation
//activityRouter.route('/search/:user_email/geo')
//        .get(function(req, res) {
//            var user_email = req.params.user_email;
//
//            var data = req.query.data;
////            console.log(req.query.data);
//
//            try {
//                var location = JSON.parse(data);
//            } catch (err) {
//                new ErrorHandler(err, res);
//            }
//            Activity.geo(user_email, location, function(err, activities) {
//                if (err)
//                    new ErrorHandler(err, res);
//                //build senders
//
//                woo_tools.activity.build_senders(activities, function(activities) {
//                    res.json(activities);
//                });
//            });
//
//        });

module.exports = activityRouter;

  