/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//Module Dependencies
var http = require('http');
var express = require('express');
var NotFoundError = require('../NotFoundError');
var ErrorHandler = require('../handler/errorHandler');

var Authentication = require('../../middleware/authentication/authentication');

var encryption = require('../../module/encryption/encryption');

//var UserModule = require('../models/user');
//var User = UserModule.model;
//
//var ActivityModule = require('../models/user');
//var Activity = ActivityModule.model;
//
//var FriendModule = require('../models/friend');
//var Friend = FriendModule.model;
//
//var NotificationModule = require('../models/notification');
//var Notification = NotificationModule.model;


//Services
var SS = require('../services/signServices');


//helpers
var woo_tools = require('../../module/wootools/wootools');

//SUB ROUTERS
var friendsRouter = require('./subRouters/friendsRouter');
var fb_actionsRouter = require('./subRouters/fb_actionsRouter');
var activityRouter = require('./subRouters/activityRouter');
//var notificationsRouter = require('./subRouters/notificationsRouter');

// ROUTES FOR OUR API
// =============================================================================

// create our router
var SignRouter = express.Router();

SignRouter.use(function(req, res, next) {
    // do logging
    console.log('MIDDLEWARE - Sign router -> .' + req.originalUrl);
    next();
});

var root = '';

SignRouter.route('/signin/:email/:pwd')
        .get(function(req, res) {

            var email = req.params.email;
            var pwd = req.params.pwd;

            var authentication = {
                email: email,
                pwd: pwd
            };

            SS.signin(authentication, function(err, user) {
                if (err)
                    new ErrorHandler(err, res);
                else
                    res.json(user);
            });

        });

SignRouter.route('/autosignin/:user_id')
        .get(function(req, res) {

            var user_id = req.params.user_id;
            var data = req.query.data;
            
            try{
                data = JSON.parse(data);
            }catch(err){
                new ErrorHandler(err, res);
            }
            
            var api_key = data.api_key;

            var authentication = {
                _id: user_id,
                api_key: api_key
            };

            SS.autoSignin(authentication, function(err, user) {
                if (err)
                    new ErrorHandler(err, res);
                else
                    res.json(user);
            });

        });
        

SignRouter.route('/signup')
        //Route to add an activity
        .post(function(req, res) {

            var userData = woo_tools.getData(req, res);

            SS.signup(userData, function(err, newuser) {
                if (err)
                    new ErrorHandler(err, res);
                else
                    res.json(newuser);
            });
        });
        

//Facebook
SignRouter.route('/fb_signin')
        //Route to add an activity
        .post(function(req, res) {

            var userData = woo_tools.getData(req, res);

            SS.fb_signin(userData, function(err, newuser) {
                if (err)
                    new ErrorHandler(err, res);
                else
                    res.json(newuser);
            });
        });
        
//Google+




module.exports = SignRouter;

  