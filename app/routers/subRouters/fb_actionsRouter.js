/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//Module Dependencies
var express = require('express');
var NotFoundError = require('../../NotFoundError');
var ErrorHandler = require('../../handler/errorHandler');


//Models
var ActivityModule = require('../../models/activity');
var Activity = ActivityModule.model;

//helpers
var woo_tools = require('../../../module/wootools/wootools');


// ROUTES - FB Actions
// =============================================================================

// create our router
var fbRouter = express.Router();

// middleware called before each routes in this router
fbRouter.use(function(req, res, next) {
    // do logging
    console.log('MIDDLEWARE - Friends router -> .' + req.originalUrl);
    next();
});

var root = '/:user_email/fb_actions';

//ROUTES -- /:user_id/friends
fbRouter.route(root)
        //Get all friends
        .get(function(req, res) {
            var usr = req.params.user_email;
            
            res.json({'data': 'hello friends'});
        });
        
       
fbRouter.route( root + '/:target_user')
        //get friend :target_user data
        .post(function(req, res) {
            var usr = req.params.user_email;
            var uid = req.params.target_user;
            
                console.log('target');
                console.log(req.params);
                
                
            res.json({
                data: 'my data',
                src: usr,
                target: uid
            });
        })

        //add :target_user as friends
        .post(function(req, res) {
            var usr = req.params.user_email;
            var uid = req.params.target_user;
            
                console.log('target');
                console.log(req.params);
                
            res.json({
                data: 'my data',
                src: usr,
                target: uid
            });
        })
        
        //delete :target_user from friends
        .delete(function(req, res) {
            var usr = req.params.user_email;
            var uid = req.params.target_user;
    
            res.json({'data': 'my data'});
        });


module.exports = fbRouter;

  