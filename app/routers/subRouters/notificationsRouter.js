/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//Module Dependencies
var express = require('express');
var NotFoundError = require('../NotFoundError');
var ErrorHandler = require('../handler/errorHandler');

var Authentication = require('../../middleware/authentication/authentication');

var NotificationModule = require('../models/notification');
var Notification = NotificationModule.model;

var UserModule = require('../models/user');
var User = UserModule.model;

var ActivityModule = require('../models/user');
var Activity = ActivityModule.model;

var wootools = require('../../module/wootools/wootools');

// ROUTES FOR OUR API
// =============================================================================

// create our router
var notificationRouter = express.Router();

// middleware called before each routes in this router
notificationRouter.use(function(req, res, next) {
    // do logging
    console.log('MIDDLEWARE - NotificationRouter -> .' + req.originalUrl);
    next();
});

notificationRouter.route('/')
        .post(function(req, res) {

            try {
                var data = JSON.parse(req.body.data);
            } catch (err) {
                new ErrorHandler(err, res);
            }

            var notification = new Notification(data);

            notification.save(function(err, notification) {
                if (err)
                    new ErrorHandler(err);
                res.json({'message': 'success'});
            });

        });

//WORK
notificationRouter.route('/:user_mail')
        .get(function(req, res) {
            var user_mail = req.params.user_mail;
    
            var build_notifs = [];

            Notification.get(user_mail, function(err, notifications) {
                if (err)
                    new ErrorHandler(err);
                  
                  res.json(notifications);
                  
                notifications.forEach(function(notif){
                    if(notif.type === 'friend'){
                        notif.build_src_user_data(function(err, build_notif){  
                            
                           build_notifs.push(build_notif);
                            
                            if(build_notifs.length === notifications.length){
                                res.json(build_notifs);
                            }
                        });
                    }
                    else{
                        build_notifs.push(notif);
                    }
                });
                
            });
        });

//WORK
notificationRouter.route('/:user_mail/:act_id')
        .get(function(req, res) {
            var user_mail = req.params.user_mail;
            var act_id = req.params.act_id;

            Notification.get(user_mail, act_id, function(err, notifications) {
                if (err)
                    new ErrorHandler(err);
                res.json(notifications);
            });
        });


notificationRouter.route('/read/:id')
        .put(function(req, res) {
            var notif_id = req.params.id;

            Notification.markAsRead(notif_id, function(err, notifications) {
                if (err)
                    new ErrorHandler(err);
                res.json({status: 'ok'});
            });
        });
        
notificationRouter.route('/pushed/:id')
        .put(function(req, res) {
            var notif_id = req.params.id;

            Notification.markAsPushed(notif_id, function(err, notifications) {
                if (err)
                    new ErrorHandler(err);
                res.json({status: 'ok'});
            });
        });        





module.exports = notificationRouter;

  