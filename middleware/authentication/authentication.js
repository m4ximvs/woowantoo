/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//var SessionHandler = require('./lib/sessionHandler');

var UserModule = require('../../app/models/user');
var User = UserModule.model;
var ErrorHandler = require('../../app/handler/errorHandler');
var encryption = require('../../module/encryption/encryption');

//the authentication middleware
var Authentication = function(user) {
    this.user = user;
};

var auth = new Authentication();

var AuthenticationHandler = function(req, res, next) {

    // parse the authentication data
    try {
        var authentication = JSON.parse(req.body.authentication);
    } catch (err) {
        new ErrorHandler(err, res);
    }


    User.signin(authentication, function(err, user) {

        if (!user || err)
            res.send('Permission denied');
        else {

            encryption.crypt(user.password, function(err, userhash) {
                console.log('Encrypt authPWD: ' + userhash);

                encryption.compare(authentication.pwd, userhash, function(err, isAllowed){
                        
                            console.log('Allowed: ' +isAllowed);
                    });

            });


            auth.user = user;
            console.log(user);
            console.log(user.email);
            next();
        }
    });
};


module.exports.store = auth;
module.exports.handler = AuthenticationHandler;