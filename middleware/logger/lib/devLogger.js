/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//config
var config = require('../../../app/config');

var devLogger = function(req, res, next) {
    
    // do logging
    console.log('FRONT MIDDLEWARE -> .'+req.originalUrl);
    console.log('APP: '+config.app);
    next();
}

module.exports = devLogger;
